from vector.vector_module import add, sub, length, multiply_by_scalar, \
    multiply, normalize, equality, equality_accuracy, cos_between, \
    oppositely_directed, co_directed, collinear, angle_between_radians, \
    angle_between_degrees, orthogonal, change_direction, projection_scalar, projection_vector
from vector.vector_view import show_vector_2d_figure_result_matplotlib
from packages.view.view_module import show_array, show_error, show_info, show_info_about_none_params
from vector.vector_converter import str_to_float_array
from packages.scalar.scalar_converter import str_to_float_number
from packages.matplotlib.matplotlib_run import handle_non_right_type_for_matplotlib_exception
from packages.calculate.try_calculate import try_calculate


def add_action():
    """Сложение векторов. Действие"""
    a, b = input_two_arrays()  # Получение данных для расчетов
    res = try_calculate(add, a, b)
    show_array(res, 'Сумма векторов')
    if handle_non_right_type_for_matplotlib_exception(res):
        try_run_matplotlib(res)


def sub_action():
    """Вычитание векторов. Действие"""
    a, b = input_two_arrays()  # Получение данных для расчетов
    res = try_calculate(sub, a, b)
    show_array(res, 'Вычитание векторов')
    if handle_non_right_type_for_matplotlib_exception(res):
        try_run_matplotlib(res)


def length_action():
    """Вычисление длины вектора. Действие"""
    a = input_one_array()  # Получение данных для расчетов
    res = try_calculate(length, a)
    show_array(res, 'Длина вектора')


def multiply_action():
    """Вычисление скалярного произведения векторов. Действие"""
    a, b = input_two_arrays()
    res = try_calculate(multiply, a, b)
    show_array(res, 'Скалярное произведение векторов')


def multiply_by_scalar_action():
    """Вычисление умножения вектора на скаляр. Действие"""
    a = input_one_array()
    b = input_one_number('Введите скаляр. Можно пропустить - [Enter]\n')
    show_info_about_none_params(b)
    res = try_calculate(multiply_by_scalar, a, b)
    show_array(res, 'Умножение на скаляр')
    if handle_non_right_type_for_matplotlib_exception(res):
        try_run_matplotlib(res)


def collinear_action():
    """Проверка на коллинеарность векторов. Действие"""
    a, b = input_two_arrays()
    c = input_one_number('Введите погрешность. Можно пропустить - [Enter]\n')
    show_info_about_none_params(c)
    res = try_calculate(collinear, a, b, c)
    show_array(res, 'Коллинеарность векторов')


def orthogonal_action():
    """Проверка на ортогнальность векторов. Действие"""
    a, b = input_two_arrays()
    c = input_one_number('Введите погрешность. Можно пропустить - [Enter]\n')
    show_info_about_none_params(c)
    res = try_calculate(orthogonal, a, b, c)
    show_array(res, 'Ортогональность векторов')


def oppositely_directed_action():
    """Проверка на противоположность векторов. Действие"""
    a, b = input_two_arrays()
    c = input_one_number('Введите погрешность. Можно пропустить - [Enter]\n')
    show_info_about_none_params(c)
    res = try_calculate(oppositely_directed, a, b, c)
    show_array(res, 'Противоположность векторов')


def change_direction_action():
    """Изменений направления вектора. Действие"""
    a = input_one_array()
    res = try_calculate(change_direction, a)
    show_array(res, 'Изменение на противоположный вектор')
    if handle_non_right_type_for_matplotlib_exception(res):
        try_run_matplotlib(res)


def co_directed_action():
    """Проверка на сонаправленность векторов. Действие"""
    a, b = input_two_arrays()
    c = input_one_number('Введите погрешность. Можно пропустить - [Enter]\n')
    show_info_about_none_params(c)
    res = try_calculate(co_directed, a, b, c)
    show_array(res, 'Сонаправленность векторов')


def equality_action():
    """Проверка на равенство векторов. Действие"""
    a, b = input_two_arrays()
    res = try_calculate(equality, a, b)
    show_array(res, 'Равенство векторов')


def cos_between_action():
    """Вычисление косинуса между векторами"""
    a, b = input_two_arrays()
    res = try_calculate(cos_between, a, b)
    show_array(res, 'Косинус угла между векторами')


def angle_between_radians_action():
    """Вычисление угла между векторами в радианах"""
    a, b = input_two_arrays()
    res = try_calculate(angle_between_radians, a, b)
    show_array(res, 'Угол между векторами в радианах')


def angle_between_degrees_action():
    """Вычисление угла между векторами в градусах"""
    a, b = input_two_arrays()
    res = try_calculate(angle_between_degrees, a, b)
    show_array(res, 'Угол между векторами в градусах')


def equality_accuracy_action():
    """Проверка на равенство векторов с заданной точностью. Действие"""
    a, b = input_two_arrays()
    c = input_one_number('Введите погрешность. Можно пропустить - [Enter]\n')
    show_info_about_none_params(c)
    res = try_calculate(equality_accuracy, a, b, c)
    show_array(res, 'Равенство векторов с заданной точностью')


def normalize_vector_action():
    """Нормализация вектора. Действие"""
    a = input_one_array()
    res = try_calculate(normalize, a)
    show_array(res, 'Нормализация вектора')
    if handle_non_right_type_for_matplotlib_exception(res):
        try_run_matplotlib(res)


def projection_scalar_action():
    """Скалярная проеция вектора на вектор. Действие"""
    a, b = input_two_arrays()
    res = try_calculate(projection_scalar, a, b)
    show_array(res, 'Скалярная проекция вектора на вектор')


def projection_vector_action():
    """Векторная проеция вектора на вектор. Действие"""
    a, b = input_two_arrays()
    res = try_calculate(projection_vector, a, b)
    show_array(res, 'Векторная проекция вектора на вектор')
    if handle_non_right_type_for_matplotlib_exception(res):
        try_run_matplotlib(res)


def try_run_matplotlib(res):
    """Проверка вызова библиотеки matplotlib. Условно Controller."""
    try:
        if len(res) == 1:
            show_vector_2d_figure_result_matplotlib(res, [0])
        elif len(res) == 2:
            show_vector_2d_figure_result_matplotlib([res[0]], [res[1]])
        else:
            return show_info('Библиотека matplotlib будет вызвана только с 2D и 1D пространствами')
    except Exception as err:
        return show_error(err, 'Ошибка при вызове библиотеки matplotlib')


def input_one_number(msg='Введите число: '):
    """Ввод одного числа с клавиатуры. Условно Controller."""
    try:
        s = input(msg)
        if s == '':
            return None
        return str_to_float_number(s)
    except ValueError as err:
        show_error(err, 'Ошибка при получении данных')
        return None


def input_one_array():
    """Ввод одного массива с клавиатуры. Условно Controller."""
    try:
        s = input('Введите массив через запятую: ')
        return str_to_float_array(s)
    except ValueError as err:
        return show_error(err, 'Ошибка при получении данных')


def input_two_arrays():
    """Ввод двух массивов с клавиатуры. Условно Controller."""
    try:
        s1 = input('Введите первый массив через запятую: ')
        s2 = input('Введите второй массив через запятую: ')
        return str_to_float_array(s1), str_to_float_array(s2)
    except ValueError as err:
        show_error(err, 'Ошибка при получении данных')
        return None, None


def get_actions():
    """Получение доступных действий"""
    return [add_action,
            sub_action,
            multiply_by_scalar_action,
            multiply_action,
            collinear_action,
            co_directed_action,
            oppositely_directed_action,
            equality_action,
            equality_accuracy_action,
            orthogonal_action,
            length_action,
            normalize_vector_action,
            change_direction_action,
            angle_between_radians_action,
            angle_between_degrees_action,
            projection_scalar_action,
            projection_vector_action,
            cos_between_action]
