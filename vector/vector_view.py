import matplotlib.pyplot as plt


def show_vector_2d_figure_result_matplotlib(u, v):
    """Вывод 2D графика на экран. View."""
    u_copy = [e for e in u]
    v_copy = [e for e in v]
    if len(u) > 1:
        u_copy[1], v_copy[0] = v_copy[0], u_copy[1]
    x = y = [0 for _ in u]
    plt.quiver(x, y, u_copy, v_copy, angles='xy', scale_units='xy', scale=1, color='red')
    plt.xlim(-max(u[0], v[0]) * 1.5 - 1, max(u[0], v[0]) * 1.5 + 1)
    plt.ylim(-max(u[0], v[0]) * 1.5 - 1, max(u[0], v[0]) * 1.5 + 1)
    plt.title('Vector')
    plt.get_current_fig_manager().set_window_title('Vector')
    plt.grid()
    plt.show()
