import math
from packages.ensure.ensure_module import ensure_value
from packages.none_to_default.none_to_default_module import none_to_default


EPS = 10e-10


def is_equal_len(a, b):
    """Равность длин"""
    return len(a) == len(b)


def ensure_is_equal_len(a, b):
    """Проверка на равенство длин векторов"""
    ensure_value(is_equal_len(a, b), 'different length of vectors')


def add(a, b):
    """Сложение векторов"""
    ensure_is_equal_len(a, b)
    return [x + y for x, y in zip(a, b)]


def sub(a, b):
    """Вычитание векторов"""
    ensure_is_equal_len(a, b)
    return [x - y for x, y in zip(a, b)]


def length(a):
    """Длина вектора"""
    return sum([e * e for e in a]) ** 0.5


def multiply_by_scalar(a, s=1):
    """Умножение вектора на скаляр"""
    s = none_to_default(s)
    return [e * s for e in a]


def multiply(a, b):
    """Умножение векторов"""
    ensure_is_equal_len(a, b)
    return sum([x * y for x, y in zip(a, b)])


def normalize(a):
    """Нормализация вектора"""
    return [e / length(a) for e in a]


def equality(a, b):
    """Равенство векторов"""
    ensure_is_equal_len(a, b)
    return False not in [x == y for x, y in zip(a, b)]


def equality_accuracy(a, b, eps=EPS):
    """Равенство векторов с заданной точностью"""
    eps = none_to_default(eps, EPS)
    ensure_is_equal_len(a, b)
    return False not in [abs(x - y) < eps for x, y in zip(a, b)]


def cos_between(a, b):
    """Косинус между векторами"""
    return multiply(a, b) / (length(a) * length(b))


def oppositely_directed(a, b, eps=EPS):
    """Противоположно направленные векторы"""
    eps = none_to_default(eps, EPS)
    return abs(cos_between(a, b) - -1) < eps


def co_directed(a, b, eps=EPS):
    """Сонаправленые векторы"""
    eps = none_to_default(eps, EPS)
    return abs(cos_between(a, b) - 1) < eps


def collinear(a, b, eps=EPS):
    """Коллинеарность векторов"""
    eps = none_to_default(eps, EPS)
    return abs(abs(cos_between(a, b)) - 1) < eps


def orthogonal(a, b, eps=EPS):
    """Ортогональность векторов"""
    eps = none_to_default(eps, EPS)
    return abs(cos_between(a, b) - 0) < eps


def angle_between_radians(a, b):
    """Угол между векторами в радианах"""
    return math.acos(cos_between(a, b))


def angle_between_degrees(a, b):
    """Угол между векторами в градусах"""
    return angle_between_radians(a, b) * 180 / math.pi


def change_direction(a):
    """Изменение направления вектора на противоположный"""
    return [-x for x in a]


def projection_scalar(a, b):
    """Скалярная проекция вектора на вектор"""
    return (multiply(a, b)) / length(b)


def projection_vector(a, b):
    """Векторная проекция вектора на вектор"""
    return multiply_by_scalar(normalize(b), projection_scalar(a, b))
