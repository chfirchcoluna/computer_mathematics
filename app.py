import vector.vector_controller as vc
import matrix.matrix_controller as mc
import sle.sle_controller as sc
import interpolation.interpolation_controller as ic
import approximation.approximation_controller as ac
import maclaurin_series.maclaurin_series_controller as msc


def select_action(functions):
    """Выбор действия из доступных"""
    is_run = True
    while is_run:
        print('\nДоступные операции:')
        for i, item in enumerate(functions):
            print(i, item.__name__)
        try:
            i = int(input('\nВыберите операцию: '))
        except ValueError as err:
            return print("\nОшибка при получении данных\n", err)
        if 0 <= i < len(functions):
            function = functions[i]
            function()
        else:
            is_run = False


def select_controller(contrllrs):
    """Выбор контроллера из доступных"""
    is_run = True
    while is_run:
        print('\nДоступные контроллеры:')
        for i, item in enumerate(contrllrs):
            print(i, item.__name__)
        try:
            i = int(input('\nВыберите контроллер: '))
        except ValueError as err:
            return print("\nОшибка при получении данных\n", err)
        if 0 <= i < len(contrllrs):
            controller = contrllrs[i]
            actions = controller.get_actions()
            select_action(actions)
        else:
            is_run = False


def get_controllers():
    """Получение доступных контроллеров"""
    return [vc, mc, sc, ic, ac, msc]


controllers = get_controllers()
select_controller(controllers)
