from sle.sle_module import solution_sle
from matrix.matrix_module import transposition, multiply_matrix
from sle.sle_module import create_extended_matrix_system
from interpolation.interpolation_module import create_a, create_b
from math import cos


def lsm_matrix_form_one_unknown(a_matrix: list, b_matrix: list, step: float = 0.25, size: float = 2.5) -> (list, list):
    """Метод наименьших квадратов в матричном виде. Один неизвестный"""
    new_a, new_b, epsilons_matrix = lsm_matrix_form(a_matrix, b_matrix)
    x = new_b[0][0] / new_a[0][0]

    data_vectors = find_coordinates_vectors_xy(x, epsilons_matrix, step, size)
    data_point = find_coordinates_points_xy(x, epsilons_matrix)

    return data_vectors, data_point


def lsm_matrix_form_two_unknowns(a_matrix: list, b_matrix: list, x_step: float = 0.3, y_step: float = 0.4,
                                 size: float = 2.5) -> (list, list):
    """Метод наименьших квадратов в матричном виде. Два неизвестных"""
    new_a, new_b, epsilons_matrix = lsm_matrix_form(a_matrix, b_matrix)
    sle, roots, logs = solution_sle(new_a, new_b)
    del sle, logs
    x = roots[0][0]
    y = roots[1][0]

    data_vectors = find_coordinates_vectors_xyz(x, y, epsilons_matrix, x_step, y_step, size)
    data_point = find_coordinates_point_xyz(x, y, epsilons_matrix)

    return data_vectors, data_point


def lsm_matrix_form(a_matrix: list, b_matrix: list) -> (list, list, list):
    """Метод наименьших квадратов в матричном виде без нахождения неизвестных"""
    a_matrix_transposition = transposition(a_matrix)
    new_a = multiply_matrix(a_matrix_transposition, a_matrix)
    new_b = multiply_matrix(a_matrix_transposition, b_matrix)

    b_changed_symbol = [[root * -1 for root in roots] for roots in b_matrix]
    epsilons_matrix = create_extended_matrix_system(a_matrix, b_changed_symbol)

    return new_a, new_b, epsilons_matrix


def find_coordinates_vectors_xy(x: float, epsilons_matrix: list, step: float, size: float) -> list:
    """Поиск координат векторов x y для отрисовки"""
    data_vectors = []
    x_i = x - size
    while x_i <= x + size:
        epsilon_list = []
        for line in epsilons_matrix:
            epsilon = line[0] * x_i + line[1]
            epsilon_list.append(epsilon * epsilon)
        data_vectors.append((x_i, sum(epsilon_list)))
        x_i += step
    return data_vectors


def find_coordinates_points_xy(x: float, epsilons_matrix: list) -> list:
    """Поиск координат точек x y для отрисовки"""
    epsilon_list = []
    for line in epsilons_matrix:
        epsilon = line[0] * x + line[1]
        epsilon_list.append(epsilon * epsilon)
    data_point = [(x, sum(epsilon_list))]
    return data_point


def find_coordinates_vectors_xyz(x: float, y: float, epsilons_matrix: list, x_step: float, y_step: float,
                                 size: float) -> list:
    """Поиск координат векторов x y z для отрисовки"""
    data_vectors = []
    x_i = x - size
    while x_i <= x + size:
        y_i = y - size
        while y_i <= y + size:
            epsilon_list = []
            for line in epsilons_matrix:
                epsilon = line[0] * x_i + line[1] * y_i + line[2]
                epsilon_list.append(epsilon * epsilon)
            data_vectors.append((x_i, y_i, sum(epsilon_list)))
            y_i += y_step
        x_i += x_step
    return data_vectors


def find_coordinates_point_xyz(x: float, y: float, epsilons_matrix: list) -> list:
    """Поиск координат точек x y z для отрисовки"""
    epsilon_list = []
    for line in epsilons_matrix:
        epsilon = line[0] * x + line[1] * y + line[2]
        epsilon_list.append(epsilon * epsilon)
    data_point = [(x, y, sum(epsilon_list))]
    return data_point


def calc_roots(data_xy: list, func=None) -> list:
    """Расчёт корней СЛАУ"""
    matrix_a = create_a(*data_xy)
    matrix_b = create_b(*data_xy)
    if func is not None:
        equation_matrix_a = func(matrix_a)
    else:
        equation_matrix_a = matrix_a
    equation_matrix_transposition = transposition(equation_matrix_a)
    new_a = multiply_matrix(equation_matrix_transposition, equation_matrix_a)
    new_b = multiply_matrix(equation_matrix_transposition, matrix_b)
    sle, roots, logs = solution_sle(new_a, new_b)
    return roots


def calc_y(roots: list, list_x: list, func=None) -> list:
    """Расчёт y по по корням СЛАУ"""
    matrix_x = create_a(*list_x)
    if func is not None:
        equation_matrix_x = func(matrix_x)
    else:
        equation_matrix_x = matrix_x
    list_y = multiply_matrix(equation_matrix_x, roots)
    return list_y


def approximation_by_2nd_degree_polynomial(data_xy: list, list_x: list, step: float = 0.3, size: float = 5) -> (
        list, list):
    """Аппроксимация полиномом 2-й степени"""
    roots, list_y = approximation_by_func(data_xy, list_x, create_equation_2nd_in_matrix_form)
    data_vectors = find_coordinates_vectors_xy_approximation(polynomial_2, list_x, roots, step, size)
    return data_vectors, list_y


def approximation_by_3nd_degree_polynomial(data_xy: list, list_x: list, step: float = 0.2, size: float = 1.5) -> (
        list, list):
    """Аппроксимация полиномом 3-й степени"""
    roots, list_y = approximation_by_func(data_xy, list_x, create_equation_3nd_in_matrix_form)
    data_vectors = find_coordinates_vectors_xy_approximation(polynomial_3, list_x, roots, step, size)
    return data_vectors, list_y


def approximation_by_arbitrary_function(data_xy: list, list_x: list, step: float = 0.2, size: float = 1.5) -> (
        list, list):
    """Аппроксимация произвольной функцией"""
    roots, list_y = approximation_by_func(data_xy, list_x, create_equation_arbitrary_in_matrix_form)
    data_vectors = find_coordinates_vectors_xy_approximation(arbitrary, list_x, roots, step, size)
    return data_vectors, list_y


def approximation_by_func(data_xy: list, list_x: list, func=None) -> (list, list):
    """Аппроксимация функцией"""
    roots = calc_roots(data_xy, func)
    list_y = calc_y(roots, list_x, func)
    return roots,  list_y


def find_coordinates_vectors_xy_approximation(func, list_x: list, roots: list, step: float, size: float) -> list:
    """Поиск координат x y векторов аппроксимации для отрисовки"""
    data_vectors = []
    x_i = min(list_x)[0] - size
    while x_i <= max(list_x)[0] + size:
        coordinate = 0
        for i in range(len(roots)):
            coordinate += roots[i][0] * func(x_i, i)
        data_vectors.append((x_i, coordinate))
        x_i += step
    return data_vectors


def polynomial_2(x: float, i: int) -> float:
    """Формула для аппроксимации полинома 2-й степени. Возврат значения по индексу"""
    return [x * x, x, 1][i]


def polynomial_3(x: float, i: int) -> float:
    """Формула для аппроксимации полинома 3-й степени. Возврат значения по индексу"""
    return [x * x * x, x * x, x, 1][i]


def arbitrary(x: float, i: int) -> float:
    """Формула для аппроксимации произвольной функции. Возврат значения по индексу"""
    return [cos(x), x, 1][i]


def create_equation_2nd_in_matrix_form(matrix: list) -> list:
    """Создание уравнения 2-й степени в виде матричной формы"""
    return [[line[0] * line[0], *line] for line in matrix]


def create_equation_3nd_in_matrix_form(matrix: list) -> list:
    """Создание уравнения 3-й степени в виде матричной формы"""
    return [[line[0] * line[0] * line[0], line[0] * line[0], *line] for line in matrix]


def create_equation_arbitrary_in_matrix_form(matrix: list) -> list:
    """Создание уравнения произвольной функции в виде матричной формы"""
    return [[cos(line[0]), *line] for line in matrix]
