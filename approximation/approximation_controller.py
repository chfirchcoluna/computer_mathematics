from packages.calculate.try_calculate import try_calculate
from packages.view.view_module import show_array, show_info
from approximation.approximation_module import lsm_matrix_form_one_unknown, lsm_matrix_form_two_unknowns,\
    approximation_by_2nd_degree_polynomial, approximation_by_3nd_degree_polynomial,\
    approximation_by_arbitrary_function, approximation_by_func
from packages.matplotlib.matplotlib_run import try_run_matplotlib, handle_non_right_type_for_matplotlib_exception
from packages.matplotlib.matplotplib_converter_data import convert_data_x_y, convert_data_x_y_z
from packages.matplotlib.matplotlib_module import show_3d_figure_result_matplotlib,\
    show_chart_2d_figure_result_matplotlib, show_chart_2d_result_matplotlib_linear_approximation,\
    show_chart_2d_figure_result_matplotlib_more_points


A_ONE = [[2],
         [3]]

B_ONE = [[4],
         [9]]

A_TWO = [[2, 3],
         [3, 3],
         [4, 7]]

B_TWO = [[7],
         [7],
         [3]]

DATA_XY = [(1, 2),
           (3, 4),
           (3.5, 3),
           (6, 7)]

LIST_X = [[1],
          [3],
          [5]]


def lsm_matrix_form_one_unknown_action():
    """Метод наименьших квадратов в матричном виде. Один неизвестный. Действие"""
    a, b = A_ONE, B_ONE

    coordinate_vectors, coordinate_point = try_calculate(lsm_matrix_form_one_unknown, a, b)

    show_array(coordinate_point, 'Решение МНК в матричном виде. Один неизвестный')

    list_xy_vectors = convert_data_x_y(coordinate_vectors)
    list_xy_point = convert_data_x_y(coordinate_point)

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_point):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib, list_xy_vectors, list_xy_point, '')


def lsm_matrix_form_two_unknowns_action():
    """Метод наименьших квадратов в матричном виде. Два неизвестных. Действие"""
    a, b = A_TWO, B_TWO

    coordinate_vectors, coordinate_point = try_calculate(lsm_matrix_form_two_unknowns, a, b)

    show_array(coordinate_point, 'Решение МНК в матричном виде. Два неизвестных')

    list_xyz_vectors = convert_data_x_y_z(coordinate_vectors)
    list_xyz_point = convert_data_x_y_z(coordinate_point)

    if handle_non_right_type_for_matplotlib_exception(list_xyz_vectors, list_xyz_point):
        try_run_matplotlib(show_3d_figure_result_matplotlib, list_xyz_vectors, list_xyz_point)


def linear_approximation_action():
    """Линейная аппроксимация. Действие"""
    data_xy, list_x = DATA_XY, LIST_X

    coordinates, list_y = try_calculate(approximation_by_func, data_xy, list_x)
    del coordinates

    show_array(list_y, 'Линейная аппроксимация. Найденные y')

    list_xy_vectors = [list_x, list_y]
    list_xy_points = convert_data_x_y(data_xy)

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points):
        if len(list_x) >= 2:
            if (list_x[0][0], list_y[0][0]) != (list_x[1][0], list_y[1][0]):
                try_run_matplotlib(show_chart_2d_result_matplotlib_linear_approximation, list_xy_vectors,
                                   list_xy_points, '')
            else:
                show_info('Невозможно провести линию через идентичных точки')
        else:
            show_info('Невозможно провести линию через точки, не зная их')


def approximation_by_2nd_degree_polynomial_action():
    """Аппроксимация полиномом 2-й степени. Действие"""
    data_xy, list_x = DATA_XY, LIST_X

    coordinate_vectors, list_y = try_calculate(approximation_by_2nd_degree_polynomial, data_xy, list_x)

    show_array(list_y, 'Аппроксимация полиномом 2-й степени. Найденные y')

    list_xy_vectors = convert_data_x_y(coordinate_vectors)
    list_xy_points = [list_x, list_y]
    list_additional_xy_points = convert_data_x_y(data_xy)

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points, list_additional_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib_more_points, list_xy_vectors, list_xy_points,
                           list_additional_xy_points, '')


def approximation_by_3nd_degree_polynomial_action():
    """Аппроксимация полиномом 3-й степени. Действие"""
    data_xy, list_x = DATA_XY, LIST_X

    coordinate_vectors, list_y = try_calculate(approximation_by_3nd_degree_polynomial, data_xy, list_x)

    show_array(list_y, 'Аппроксимация полиномом 3-й степени. Найденные y')

    list_xy_vectors = convert_data_x_y(coordinate_vectors)
    list_xy_points = [list_x, list_y]
    list_additional_xy_points = convert_data_x_y(data_xy)

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points, list_additional_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib_more_points, list_xy_vectors, list_xy_points,
                           list_additional_xy_points, '')


def approximation_by_arbitrary_function_action():
    """Аппроксимация произвольной функцией. Действие"""
    data_xy, list_x = DATA_XY, LIST_X

    coordinate_vectors, list_y = try_calculate(approximation_by_arbitrary_function, data_xy, list_x)

    list_xy_vectors = convert_data_x_y(coordinate_vectors)
    list_xy_points = [list_x, list_y]
    list_additional_xy_points = convert_data_x_y(data_xy)

    show_array(list_y, 'Аппроксимация полиномом произвольной функцией. Найденные y')

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points, list_additional_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib_more_points, list_xy_vectors, list_xy_points,
                           list_additional_xy_points, '')


def get_actions():
    """Получение доступных действий"""
    return [lsm_matrix_form_one_unknown_action,
            lsm_matrix_form_two_unknowns_action,
            linear_approximation_action,
            approximation_by_2nd_degree_polynomial_action,
            approximation_by_3nd_degree_polynomial_action,
            approximation_by_arbitrary_function_action]
