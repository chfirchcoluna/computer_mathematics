from packages.ensure.ensure_module import ensure_value
from sle.sle_module import solution_sle
from math import inf


def linear_interpolation(coordinate_0: tuple, coordinate_1: tuple, x: float):
    """Линейная интерполяция"""
    ensure_is_x_in_range(coordinate_0, coordinate_1, x)
    return approximation(coordinate_0, coordinate_1, x)


def linear_extrapolation(coordinate_0: tuple, coordinate_1: tuple, x: float):
    """Линейная экстраполяция"""
    ensure_is_x_not_in_range(coordinate_0, coordinate_1, x)
    return approximation(coordinate_0, coordinate_1, x)


def approximation(coordinate_0: tuple, coordinate_1: tuple, x: float = 0):
    """Линейная аппроксимация"""
    matrix_a = create_a(coordinate_0, coordinate_1)
    matrix_b = create_b(coordinate_0, coordinate_1)
    sle, roots, logs = solution_sle(matrix_a, matrix_b)
    del logs, sle
    a, b = roots[0][0], roots[1][0]
    y = a * x + b
    return (x * a, y), x, y, a, b


def piecewise_linear_interpolation(data_xy: list, list_x: list):
    """Кусочно-линейная интерполяция"""
    for x in list_x:
        ensure_is_x_in_range(min(data_xy), max(data_xy), x)
    return piecewise_linear_approximation(data_xy, list_x)


def piecewise_linear_extrapolation(data_xy: list, list_x: list):
    """Кусочно-линейная экстраполяция"""
    for x in list_x:
        ensure_is_x_not_in_range(min(data_xy), max(data_xy), x)
    return piecewise_linear_approximation(data_xy, list_x)


def piecewise_linear_approximation(data_xy: list, list_x: list):
    """Кусочно-линейная аппроксимация"""
    a_and_b_for_equations = create_a_and_b_for_equations(data_xy)
    intervals = create_intervals(data_xy)
    list_xy = find_list_xy_for_x(list_x, a_and_b_for_equations, intervals)
    return list_xy


def lagrange_interpolation_polynomial(data_xy: list, step: float = 0.1):
    """Интерполяционный полином Лагранжа"""
    n = len(data_xy)
    x = min(data_xy)[0]
    points = []
    ylx_list = []
    while x <= max(data_xy)[0]:
        ylx_list.clear()
        for i in range(n):
            lx = 1
            for j in range(n):
                if i != j:
                    lx *= (x - data_xy[j][0]) / (data_xy[i][0] - data_xy[j][0])
            ylx_list.append(data_xy[i][1] * lx)
        points.append((x, sum(ylx_list)))
        x += step
    return points


def create_a_and_b_for_equations(data_xy: list):
    """Нахождение коэффициетов a и b уравнений"""
    return [approximation(data_xy[equation_i - 1], data_xy[equation_i])[3:5] for equation_i in range(1, len(data_xy))]


def find_list_xy_for_x(list_x: list, ab: list, intervals: list):
    """Нахождение y для каждого икса"""
    list_xy = []
    for x in list_x:
        for index in range(len(intervals)):
            if intervals[index][0] <= x < intervals[index][1]:
                y = ab[index][0] * x + ab[index][1]
                list_xy.append((x, y))
    return list_xy


def create_intervals(data_xy: list):
    """Создание интервалов для использования уравнений"""
    intervals = [[data_xy[interval - 1][0], data_xy[interval][0]] for interval in range(1, len(data_xy))]
    intervals[0][0], intervals[-1][1] = -inf, inf
    return intervals


def ensure_is_x_in_range(coordinate_0: tuple, coordinate_1: tuple, x: float):
    """Проверка на нахождение икса в диапазоне иксов координат"""
    ensure_value(is_x_in_range(coordinate_0, coordinate_1, x), 'x is not in the range')


def ensure_is_x_not_in_range(coordinate_0: tuple, coordinate_1: tuple, x: float):
    """Проверка на нахождение икса не в диапазоне иксов координат"""
    ensure_value(not is_x_in_range(coordinate_0, coordinate_1, x), 'x is in the range')


def is_x_in_range(coordinate_0: tuple, coordinate_1: tuple, x: float):
    """Икс в диапазоне иксов координат"""
    return min(coordinate_0[0], coordinate_1[0]) <= x <= max(coordinate_0[0], coordinate_1[0])


def create_a(*coordinates: tuple):
    """Создание матрицы А"""
    return [[coordinate[0], 1] for coordinate in coordinates]


def create_b(*coordinates: tuple):
    """Создание матрицы b"""
    return [[coordinate[1]] for coordinate in coordinates]
