from packages.view.view_module import show_array, show_info
from interpolation.interpolation_module import linear_interpolation, linear_extrapolation, approximation,\
    piecewise_linear_interpolation, piecewise_linear_extrapolation, piecewise_linear_approximation, \
    lagrange_interpolation_polynomial
from packages.matplotlib.matplotlib_module import show_chart_2d_figure_result_matplotlib
from packages.matplotlib.matplotplib_converter_data import convert_data_x_y
from packages.matplotlib.matplotlib_run import handle_non_right_type_for_matplotlib_exception, try_run_matplotlib
from packages.calculate.try_calculate import try_calculate
from packages.round.round_module import deep_round_points
from packages.list.list_converter import deep_convert_list_to_tuple


DATA_XY = [(1, 2),
           (3, 4),
           (3.5, 3),
           (6, 7)]

X = [-1.5,
     3,
     2,
     5,
     9]

X_INTERPOLATION = [3,
                   2,
                   5]

X_EXTRAPOLATION = [-1.5,
                   9]

COORDINATE_A = (2, 5)

COORDINATE_B = (6, 9)

X_0 = 3

X_1 = 12


def linear_interpolation_action():
    """Решение линейной интерполяции. Действие."""
    coordinate_a, coordinate_b, x_1 = COORDINATE_A, COORDINATE_B, X_0  # Получение данных для расчетов
    point1, x1, y1, a, b = try_calculate(linear_interpolation, coordinate_a, coordinate_b, x_1)
    point1_round, x1_round, y1_round, a_round, b_round = deep_round_points(point1), deep_round_points(
        x1), deep_round_points(y1), deep_round_points(a), deep_round_points(b)
    point1_round_tuple = deep_convert_list_to_tuple(point1_round)
    show_array(point1_round_tuple, 'Решение линейной интерполяции')
    show_info(f'y = ax + b: \n{y1_round} = {a_round} * {x1_round} + {b_round}')
    list_xy_vectors = convert_data_x_y(coordinate_a, coordinate_b)
    list_xy_points = convert_data_x_y(point1)
    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib, list_xy_vectors, list_xy_points)


def linear_extrapolation_action():
    """Решение линейной экстраполяции. Действие."""
    coordinate_a, coordinate_b, x_1 = COORDINATE_A, COORDINATE_B, X_1  # Получение данных для расчетов
    point1, x1, y1, a, b = try_calculate(linear_extrapolation, coordinate_a, coordinate_b, x_1)
    point1_round, x1_round, y1_round, a_round, b_round = deep_round_points(point1), deep_round_points(
        x1), deep_round_points(y1), deep_round_points(a), deep_round_points(b)
    point1_round_tuple = deep_convert_list_to_tuple(point1_round)
    show_array(point1_round_tuple, 'Решение линейной экстраполяции')
    show_info(f'y = ax + b: \n{y1_round} = {a_round} * {x1_round} + {b_round}')
    list_xy_vectors = try_calculate(convert_data_x_y, coordinate_a, coordinate_b)
    list_xy_points = try_calculate(convert_data_x_y, point1)
    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib, list_xy_vectors, list_xy_points)


def linear_approximation_action():
    """Решение линейной аппроксимации. Действие."""
    coordinate_a, coordinate_b, x_1, x_2 = COORDINATE_A, COORDINATE_B, X_0, X_1  # Получение данных для расчетов
    point1, x1, y1, a, b = try_calculate(approximation, coordinate_a, coordinate_b, x_1)
    point2, x2, y2, a, b = try_calculate(approximation, coordinate_a, coordinate_b, x_2)
    point1_round, point2_round, a_round, b_round = deep_round_points(point1), deep_round_points(
        point2), deep_round_points(a), deep_round_points(b)
    point1_round_tuple = deep_convert_list_to_tuple(point1_round)
    point2_round_tuple = deep_convert_list_to_tuple(point2_round)
    show_info('Решение линейной аппроксимации')
    show_array(point1_round_tuple, point2_round_tuple)
    show_info(f'y = ax + b: \ny = {a_round} * x + {b_round}')
    del x1, x2, y1, y2
    list_xy_vectors = try_calculate(convert_data_x_y, coordinate_a, coordinate_b)
    list_xy_points = try_calculate(convert_data_x_y, point1, point2)
    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib, list_xy_vectors, list_xy_points)


def piecewise_linear_interpolation_action():
    """Решение кусочно-линейной интерполяции. Действие."""
    data_xy, x = DATA_XY, X_INTERPOLATION  # Получение данных для расчетов
    res_xy = try_calculate(piecewise_linear_interpolation, data_xy, x)
    res_xy_round = deep_round_points(res_xy)
    res_xy_round_tuple = deep_convert_list_to_tuple(res_xy_round)
    show_array(res_xy_round_tuple, 'Решение кусочно-линейной интерполяции')
    list_xy_vectors = try_calculate(convert_data_x_y, data_xy)
    list_xy_points = try_calculate(convert_data_x_y, res_xy)
    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib, list_xy_vectors, list_xy_points)


def piecewise_linear_extrapolation_action():
    """Решение кусочно-линейной экстраполяции. Действие."""
    data_xy, x = DATA_XY, X_EXTRAPOLATION  # Получение данных для расчетов
    res_xy = try_calculate(piecewise_linear_extrapolation, data_xy, x)
    res_xy_round = deep_round_points(res_xy)
    res_xy_round_tuple = deep_convert_list_to_tuple(res_xy_round)
    show_array(res_xy_round_tuple, 'Решение кусочно-линейной интерполяции')
    list_xy_vectors = try_calculate(convert_data_x_y, data_xy)
    list_xy_points = try_calculate(convert_data_x_y, res_xy)
    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib, list_xy_vectors, list_xy_points)


def piecewise_linear_approximation_action():
    """Решение кусочно-линейной аппроксимации. Действие."""
    data_xy, x = DATA_XY, X  # Получение данных для расчетов
    res_xy = try_calculate(piecewise_linear_approximation, data_xy, x)
    res_xy_round = deep_round_points(res_xy)
    res_xy_round_tuple = deep_convert_list_to_tuple(res_xy_round)
    show_array(res_xy_round_tuple, 'Решение кусочно-линейной интерполяции')
    list_xy_vectors = try_calculate(convert_data_x_y, data_xy)
    list_xy_points = try_calculate(convert_data_x_y, res_xy)
    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib, list_xy_vectors, list_xy_points)


def lagrange_interpolation_polynomial_action():
    """Решение интерполяционного полинома Лагранжа. Действие"""
    data_xy = DATA_XY
    res = try_calculate(lagrange_interpolation_polynomial, data_xy)
    list_xy_vectors = try_calculate(convert_data_x_y, res)
    list_xy_points = try_calculate(convert_data_x_y, data_xy)
    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_points):
        try_run_matplotlib(show_chart_2d_figure_result_matplotlib, list_xy_vectors, list_xy_points, '')


def get_actions():
    """Получение доступных действий"""
    return [linear_interpolation_action,
            linear_extrapolation_action,
            linear_approximation_action,
            piecewise_linear_interpolation_action,
            piecewise_linear_extrapolation_action,
            piecewise_linear_approximation_action,
            lagrange_interpolation_polynomial_action]
