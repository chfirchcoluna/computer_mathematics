from matrix.matrix_module import get_row, rearranging_rows, sub_row_multiplied_by_scalar,\
    ensure_is_equal_len, ensure_need_do_copy_matrix, create_extended_matrix_system, multiply_matrix
from vector.vector_module import multiply_by_scalar, EPS
from packages.copy.deep_copy_module import deep_copy_matrix
from packages.ensure.ensure_module import ensure_is_not_none


LOGS = []


def solution_sle(a, b):
    """Решение СЛАУ"""
    LOGS.clear()

    # Проверка равества длины матрицы A и b
    ensure_is_equal_len(a, b)
    LOGS.append([a, 'Принятая матрица A'])
    LOGS.append([b, 'Принятая матрица b'])

    # Количество стоблоцв матрицы A
    number_columns_matrix_a = len(a[0])

    # Количество столбцов матрицы b
    number_columns_matrix_b = len(b[0])

    # Расширенная СЛАУ
    extended_matrix_system = create_extended_matrix_system(a, b)
    LOGS.append([extended_matrix_system, 'Создание расширенной СЛАУ'])

    final_ems, last_column_ems = selection_leading_elements_on_main_diagonal_system(extended_matrix_system,
                                                                                    number_columns_matrix_a,
                                                                                    number_columns_matrix_b)
    # Возврат решения путём выбора ведущих элементов на главной диагонали матрицы
    return final_ems, last_column_ems, LOGS


def selection_leading_elements_on_main_diagonal_system(ems_arg, number_column_matrix, number_columns_answer,
                                                       do_copy=True):
    """Решение путём выбора ведущих элементов на главной диагонали матрицы"""
    ems = ensure_need_do_copy_matrix(ems_arg, do_copy)
    for index_column in range(number_column_matrix):
        ems = selection_leading_element(ems, index_column)
        LOGS.append([ems, f'Приведение к нулю элементов {index_column} столбца матрицы сверху и/или снизу'])
    is_has_solution = check_has_solution(ems)
    ensure_is_not_none(is_has_solution)
    last_columns = roots_sle(ems, number_columns_answer)
    return ems, last_columns


def check_has_solution(ems, eps=EPS):
    """Проверка есть ли решение"""
    for index_raw in range(len(ems)):
        counter = 0
        for element in range(len(ems[index_raw]) - 1):
            if ems[index_raw][element] != 0:
                counter += 1
        if counter == 0 and ems[index_raw][-1] > eps:
            return
    return True


def roots_sle(ems_arg, number_columns=1, do_copy=True):
    """Корни СЛАУ"""
    ems = ensure_need_do_copy_matrix(ems_arg, do_copy)
    x = []
    for index in range(len(ems)):
        x.append([])
        for column in range(number_columns, 0, -1):
            x[index].append(ems[index][-column])
    return x


def selection_leading_element(ems_arg, index_column, do_copy=True):
    """Решение путём выбора ведущего элемента на главной диагонали матрицы"""

    ems = ensure_need_do_copy_matrix(ems_arg, do_copy)

    # Система без ведущего нуля выбраной строки и смена строк по необходимости
    ems = ems_without_zero_leading_element(ems, index_column)

    # Проверка ведущего элемента на нулевое значение
    is_leading_element_zero = check_element_is_zero(get_row(ems, index_column), index_column)
    if not is_leading_element_zero:

        # Деление строки матрицы на ведущий элемент
        ems = division_ems_row_on_leading_element(ems, index_column)

        # Приведение к нулю верхних и нижних элементов от ведущего
        ems = ems_range_to_zero(ems, index_column, len(ems))

    return ems


def ems_range_to_zero(ems_arg, index_column, length_ems, do_copy=True):
    """Приведение к нулю верхних и нижних элементов от ведущего"""
    ems = ensure_need_do_copy_matrix(ems_arg, do_copy)

    # Приведение к нулю нижних элементов
    ems = range_to_zero(ems, index_column, index_column + 1, length_ems)

    # Приведение к нулю верхних элементов
    ems = range_to_zero(ems, index_column, index_column - 1, -1, -1)

    return ems


def division_ems_row_on_leading_element(ems_arg, index_column, do_copy=True):
    """Деление строки матрицы на ведущий элемент"""
    ems = ensure_need_do_copy_matrix(ems_arg, do_copy)

    # Перессылка в строку: деление на ведущий элемент
    ems[index_column] = multiply_by_scalar(get_row(ems, index_column), 1 / row_element(ems, index_column, index_column))

    LOGS.append([ems, f'Деление {index_column} строки на ведущий элемент'])
    return ems


def check_element_is_zero(row, index):
    """Проверка элемента строки на нулевое значение"""
    return row[index] == 0


def ems_without_zero_leading_element(ems, index_row):
    """Вычисление расширенной СЛАУ без нулевего ведущего элемента путём перестановки строк"""
    is_leading_element_zero = check_element_is_zero(get_row(ems, index_row), index_row)
    if is_leading_element_zero:
        return find_index_and_rearranging_rows(ems, index_row)
    return ems


def find_index_and_rearranging_rows(matrix, index_start):
    """Поиск индекса и перестановка строк"""
    find_index_row = find_index_row_with_non_zero_leading_element(matrix, index_start)
    is_line_full_zero = check_line_on_zeros(get_row(matrix, index_start))
    if not is_line_full_zero:
        ensure_is_not_none(find_index_row)
        if find_index_row > index_start:
            new_matrix = rearranging_rows(matrix, index_start, find_index_row)
            LOGS.append([new_matrix, f'Смена строки матрицы {index_start} со строкой {find_index_row}'])
            return new_matrix
    return matrix


def check_line_on_zeros(row, eps=EPS):
    """Проверка вся ли строка около нулевая"""
    for element in row:
        if element > eps:
            return False
    return True


def find_index_row_with_non_zero_leading_element(matrix, start_index):
    """Поиск индекса строки не начинающейся с 0"""
    index = find_index_row_in_range_matrix(matrix, start_index, start_index + 1, len(matrix))
    if index is None:
        return find_index_row_in_range_matrix(matrix, start_index, start_index - 1, -1, -1)
    return index


def find_index_row_in_range_matrix(matrix, start_index, start, stop, step=1):
    """Поиск индекса строки в диапазоне матрицы"""
    for index in range(start, stop, step):
        is_leading_element_zero = check_element_is_zero(get_row(matrix, index), start_index)
        if not is_leading_element_zero:
            return index


def range_to_zero(matrix_arg, index, start, stop, step=1, do_copy=True):
    """Приведение ведущего элемента в заданнам диапазоне к нулю"""
    matrix = ensure_need_do_copy_matrix(matrix_arg, do_copy)
    for index_row in range(start, stop, step):
        matrix = sub_row_multiplied_by_scalar(matrix, index_row, index, row_element(matrix, index_row, index))
    return matrix


def row_element(matrix, index_row, index_column, do_copy=True):
    """Получение элемента по индексу"""
    if do_copy:
        matrix_copied = deep_copy_matrix(matrix)
        return matrix_copied[index_row][index_column]
    return matrix[index_row][index_column]


def solution_sle_by_inverse_matrix(inverse_matrix, b):
    """Решение СЛАУ с помощью обратной матрицы"""
    return multiply_matrix(inverse_matrix, b)


def unit_matrix(matrix):
    """Построение единичной матрицы в зависимости от размерности первой матрицы"""
    return [[int(row == column) for column in range(len(matrix))] for row in range(len(matrix))]
