from sle.sle_module import solution_sle, solution_sle_by_inverse_matrix, unit_matrix
from packages.view.view_module import show_array, show_error
from sle.sle_view import show_logs, show_sle
from packages.calculate.try_calculate import try_calculate


MATRIX_A = [[-1, 2, 6],
            [3, -6, 0],
            [1, 0, 6]]

MATRIX_b = [[15],
            [-9],
            [5]]

MATRIX_B = [[1, 2],
            [3, 4]]

MATRIX_C = [[-2.0, 1.0],
            [1.5, -0.5]]

MATRIX_d = [[6],
            [8]]


def solution_sle_action():
    """Решение СЛАУ. Действие."""
    a, b = MATRIX_A, MATRIX_b  # Получение данных для расчетов
    is_show_logs = input_true_or_false('Нужно вывести логи или нет? [True/False]. Можно пропустить - [Enter]\n')
    res = try_calculate(solution_sle, a, b)
    if res is not None:
        res, roots, logs = res[0], res[1], res[2]
        show_sle(res, 'Решение СЛАУ')
        show_array(roots, 'Обратная матрица')
        if is_show_logs:
            show_logs(logs)


def finding_inverse_matrix_by_sle_action():
    """Поиск обратной матрицы методом Гаусса-Жордана"""
    a = MATRIX_B
    b = try_calculate(unit_matrix, a)
    is_show_logs = input_true_or_false('Нужно вывести логи или нет? [True/False]. Можно пропустить - [Enter]\n')
    res = try_calculate(solution_sle, a, b)
    if res is not None:
        res, roots, logs = res[0], res[1], res[2]
        show_sle(res, 'Решение СЛАУ')
        show_array(roots, 'Обратная матрица')
        if is_show_logs:
            show_logs(logs)


def solution_sle_by_inverse_matrix_action():
    """Решение СЛАУ с помощью обратной матрицы"""
    a, b = MATRIX_C, MATRIX_d
    res = try_calculate(solution_sle_by_inverse_matrix, a, b)
    show_sle(res, 'Решение СЛАУ с помощью обратной матрицы')


def input_true_or_false(msg=' '):
    """Ввод одного числа с клавиатуры. Условно Controller."""
    try:
        s = input(msg)
        if s == 'False':
            return False
        elif s == 'True':
            return True
        else:
            return None
    except ValueError as err:
        return show_error(err, 'Ошибка при получении данных')


def get_actions():
    """Получение доступных действий"""
    return [solution_sle_action,
            finding_inverse_matrix_by_sle_action,
            solution_sle_by_inverse_matrix_action]
