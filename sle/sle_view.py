COMMENT = 1
ROW = 0


def show_sle(ems, msg=''):
    """Вывод СЛАУ на экран"""
    if ems is not None:
        print(msg)
        for row in ems:
            print(row)


def show_logs(logs):
    """Вывод логов на экран"""
    if logs is None:
        return
    print('-' * 80)
    print('Logs start')
    print('-' * 80)
    for log in logs:
        print()
        print(log[COMMENT])
        for row in range(len(log[ROW])):
            line = ''
            for i in range(len(log[ROW][row])):
                line += str("%8.3f" % log[ROW][row][i]) + "  |  "
            print(" | " + line)
    print()
    print('-' * 80)
    print('Logs end')
    print('-' * 80)
