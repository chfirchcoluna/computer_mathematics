from math import pi


def maclaurin_series_expansion(func, n: int, x_min: float = -5, x_max: float = 6, step: float = 0.1):
    """Разложение в ряд Маклорена. Нахождение координат"""
    points = []
    x = x_min
    while x <= x_max:
        series = []
        for index_member_series in range(n + 1):
            member = func(x, index_member_series)
            series.append(member)
        if func != process_arc_cos:
            points.append((x, sum(series)))
        else:
            points.append((x, pi/2 - sum(series)))
        x += step
    return points


def process_ex(x: float, n: int):
    """Рассчёт члена ряда e^x"""
    return (x ** n) / factorial(n)


def process_sin(x: float, n: int):
    """Рассчёт члена ряда sin(x)"""
    return ((x ** (2 * n + 1)) * (-1) ** n) / factorial(2 * n + 1)


def process_cos(x: float, n: int):
    """Рассчёт члена ряда cos(x)"""
    return ((x ** (2 * n)) * (-1) ** n) / factorial(2 * n)


def process_arc_sin(x: float, n: int):
    """Рассчёт члена ряда arcsin(x)"""
    return (factorial(2 * n) * (x ** (2 * n + 1))) / ((4 ** n) * factorial(n) * factorial(n) * (2 * n + 1))


def process_arc_cos(x: float, n: int):
    """Рассчёт члена ряда arccos(x)"""
    return process_arc_sin(x, n)


def factorial(n):
    """Вычисление факториала"""
    if n == 0:
        return 1
    return factorial(n - 1) * n
