from packages.calculate.try_calculate import try_calculate
from packages.view.view_module import show_info
from maclaurin_series.maclaurin_series_module import maclaurin_series_expansion, process_ex, process_sin, process_cos,\
    process_arc_sin, process_arc_cos
from packages.matplotlib.matplotlib_module import show_two_chart_2d_figure_result_matplotlib
from packages.matplotlib.matplotlib_run import try_run_matplotlib, handle_non_right_type_for_matplotlib_exception
from packages.matplotlib.matplotplib_converter_data import convert_data_x_y

N = 25
N_1 = 2
N_2 = 5
N_3 = 0


def maclaurin_series_expansion_ex_action():
    """Разложение в ряд Маклорена e^x"""
    n = N_1

    coordinate_vectors = try_calculate(maclaurin_series_expansion, process_ex, n)
    coordinate_vectors_original = try_calculate(maclaurin_series_expansion, process_ex, N)

    show_info('Решение ряда Маклорена e^x')

    list_xy_vectors = convert_data_x_y(coordinate_vectors)
    list_xy_original = convert_data_x_y(coordinate_vectors_original)

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_original):
        try_run_matplotlib(show_two_chart_2d_figure_result_matplotlib, list_xy_original, list_xy_vectors, '', '')


def maclaurin_series_expansion_cos_action():
    """Разложение в ряд Маклорена cos(x)"""
    n = N_2

    coordinate_vectors = try_calculate(maclaurin_series_expansion, process_cos, n, -6)
    coordinate_vectors_original = try_calculate(maclaurin_series_expansion, process_cos, N, -6)

    show_info('Решение ряда Маклорена cos(x)')

    list_xy_vectors = convert_data_x_y(coordinate_vectors)
    list_xy_original = convert_data_x_y(coordinate_vectors_original)

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_original):
        try_run_matplotlib(show_two_chart_2d_figure_result_matplotlib, list_xy_original, list_xy_vectors, '', '')


def maclaurin_series_expansion_sin_action():
    """Разложение в ряд Маклорена sin(x)"""
    n = N_1

    coordinate_vectors = try_calculate(maclaurin_series_expansion, process_sin, n, -6)
    coordinate_vectors_original = try_calculate(maclaurin_series_expansion, process_sin, N, -6)

    show_info('Решение ряда Маклорена sin(x)')

    list_xy_vectors = convert_data_x_y(coordinate_vectors)
    list_xy_original = convert_data_x_y(coordinate_vectors_original)

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_original):
        try_run_matplotlib(show_two_chart_2d_figure_result_matplotlib, list_xy_original, list_xy_vectors, '', '')


def maclaurin_series_expansion_arc_sin_action():
    """Разложение в ряд Маклорена sin(x)"""
    n = N_3

    coordinate_vectors = try_calculate(maclaurin_series_expansion, process_arc_sin, n, -6)
    coordinate_vectors_original = try_calculate(maclaurin_series_expansion, process_arc_sin, N, -6)

    show_info('Решение ряда Маклорена arcsin(x)')

    list_xy_vectors = convert_data_x_y(coordinate_vectors)
    list_xy_original = convert_data_x_y(coordinate_vectors_original)

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_original):
        try_run_matplotlib(show_two_chart_2d_figure_result_matplotlib, list_xy_original, list_xy_vectors, '', '')


def maclaurin_series_expansion_arc_cos_action():
    """Разложение в ряд Маклорена cos(x)"""
    n = N_1

    coordinate_vectors = try_calculate(maclaurin_series_expansion, process_arc_cos, n, -6)
    coordinate_vectors_original = try_calculate(maclaurin_series_expansion, process_arc_cos, N, -6)

    show_info('Решение ряда Маклорена arccos(x)')

    list_xy_vectors = convert_data_x_y(coordinate_vectors)
    list_xy_original = convert_data_x_y(coordinate_vectors_original)

    if handle_non_right_type_for_matplotlib_exception(list_xy_vectors, list_xy_original):
        try_run_matplotlib(show_two_chart_2d_figure_result_matplotlib, list_xy_original, list_xy_vectors, '', '')


def get_actions():
    """Получение доступных действий"""
    return [maclaurin_series_expansion_ex_action,
            maclaurin_series_expansion_cos_action,
            maclaurin_series_expansion_sin_action,
            maclaurin_series_expansion_arc_sin_action,
            maclaurin_series_expansion_arc_cos_action]
