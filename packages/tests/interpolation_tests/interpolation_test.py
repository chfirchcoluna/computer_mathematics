import interpolation.interpolation_module as inn
from packages.round.round_module import deep_round_points
from packages.list.list_converter import deep_convert_list_to_tuple


def test_linear_interpolation():
    """Тест линейной интерполяции"""
    pa = (3, 3)
    pb = (7, 7)
    exp = (5, 5)
    res, x, y, a, b = inn.linear_interpolation(pa, pb, 5)
    res_round = deep_round_points(res, 0)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp


def test_linear_extrapolation():
    """Тест линейной экстраполяции"""
    pa = (3, 3)
    pb = (5, 5)
    exp = (7, 7)
    res, x, y, a, b = inn.linear_extrapolation(pa, pb, 7)
    res_round = deep_round_points(res, 0)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp


def test_linear_approximation():
    """Тест линейной аппроксимации"""
    pa = (3, 3)
    pb = (7, 7)
    exp = (5, 5)
    res, x, y, a, b = inn.approximation(pa, pb, 5)
    res_round = deep_round_points(res, 0)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp
    exp = (12, 12)
    res, x, y, a, b = inn.approximation(pa, pb, 12)
    res_round = deep_round_points(res, 0)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp


def test_piecewise_linear_interpolation():
    """Тест кусочно-линейной интерполяции"""
    data_xy = [(0, -1),
               (2, 0.2),
               (3, 0.5),
               (3.5, 0.8)]
    x = [1,
         3.2]
    exp = ((1, -0.4), (3.2, 0.62))
    res = inn.piecewise_linear_interpolation(data_xy, x)
    res_round = deep_round_points(res, 2)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp


def test_piecewise_linear_extrapolation():
    """Тест кусочно-линейной экстраполяции"""
    data_xy = [(0, 0),
               (5, 5),
               (10, 5)]
    x = [-2,
         17]
    exp = ((-2, -2), (17, 5))
    res = inn.piecewise_linear_extrapolation(data_xy, x)
    res_round = deep_round_points(res, 0)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp


def test_piecewise_linear_approximation():
    """Тест кусочно-линейной аппроксимации"""
    data_xy = [(0, 0),
               (5, 5),
               (10, 5)]
    x = [-5,
         7]
    exp = ((-5, -5), (7, 5))
    res = inn.piecewise_linear_approximation(data_xy, x)
    res_round = deep_round_points(res, 1)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp


def test_lagrange_interpolation_polynomial():
    """Тест интерполяционный полином Лагранжа"""
    data_xy = [(1, 2),
               (3, 4),
               (3.5, 3),
               (6, 7)]
    exp = ((1, 2.0), (1.5, 4.12), (2.0, 4.92), (2.5, 4.76), (3.0, 4.0), (3.5, 3.0), (4.0, 2.12), (4.5, 1.72),
           (5.0, 2.16), (5.5, 3.8), (6.0, 7.0))
    res = inn.lagrange_interpolation_polynomial(data_xy, 0.5)
    res_round = deep_round_points(res, 2)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp
