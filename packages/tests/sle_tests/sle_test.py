import sle.sle_module as sle
import matrix.matrix_module as mtx


def test_solution_sle_0():
    """Тест матрицы 3 на 3. СЛАУ"""
    a = [[1, 1, 1],
         [2, 2, 2],
         [3, 3, 3]]
    b = [[1],
         [2],
         [3]]
    exp = [[1, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]]
    res, x, logs = sle.solution_sle(a, b)
    assert res == exp


def test_solution_sle_1():
    """Тест матрицы 3 на 3. СЛАУ"""
    a = [[1, 1, 1],
         [2, 1, 1],
         [3, 2, 2]]
    b = [[1],
         [2],
         [3]]
    exp = [[1, 0, 0, 1], [0, 1, 1, 0], [0, 0, 0, 0]]
    res, x, logs = sle.solution_sle(a, b)
    assert res == exp


def test_solution_sle_2():
    """Тест матрицы 3 на 3. СЛАУ"""
    a = [[-1, 2, 6],
         [3, -6, 0],
         [1, 0, 6]]
    b = [[15],
         [-9],
         [5]]
    exp = [[1, 0, 0, -7], [0, 1, 0, -2], [0, 0, 1, 2]]
    res, x, logs = sle.solution_sle(a, b)
    assert res == exp
    res = mtx.multiply_matrix(a, x)
    assert res == b


def test_solution_sle_3():
    """Тест матрицы 2 на 2. СЛАУ"""
    a = [[2, 3],
         [4, 3]]
    b = [[2],
         [7]]
    exp = [[1, 0, 2.5], [0, 1, -1]]
    res, x, logs = sle.solution_sle(a, b)
    assert res == exp
    res = mtx.multiply_matrix(a, x)
    assert res == b


def test_finding_inverse_matrix_by_sle():
    """Тест нахождения обратной матрицы с помощью СЛАУ"""
    a = [[1, 2],
         [3, 4]]
    b = [[1, 0],
         [0, 1]]
    exp = [[-2, 1],
           [1.5, -0.5]]
    solution, res, logs = sle.solution_sle(a, b)
    assert res == exp
    res = mtx.multiply_matrix(a, res)
    exp = b
    assert res == exp


def test_solution_sle_by_inverse_matrix_action():
    """Решение СЛАУ с помощью обратной матрицы"""
    a = [[-2.0, 1.0],
         [1.5, -0.5]]
    b = [[6],
         [8]]
    c = [[1, 2],
         [3, 4]]
    exp = [[-4],
           [5]]
    res = sle.solution_sle_by_inverse_matrix(a, b)
    assert res == exp
    res = mtx.multiply_matrix(c, res)
    exp = b
    assert res == exp
