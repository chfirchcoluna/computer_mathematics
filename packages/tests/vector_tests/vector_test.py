import vector.vector_module as vec


def test_add():
    """Тест сложения векторов"""
    a = [1, 2, 5]
    b = [4, 8, 1]
    exp = [5, 10, 6]
    res = vec.add(a, b)
    assert res == exp


def test_sub():
    """Тест вычитания векторов"""
    a = [1, 2, 5]
    b = [4, 8, 1]
    exp = [-3, -6, 4]
    res = vec.sub(a, b)
    assert res == exp


def test_length():
    """Тест длины вектора"""
    a = [1, -3, 3, -1]
    exp = 2 * 5 ** 0.5
    res = vec.length(a)
    assert res == exp


def test_multiply_scalar():
    """Тест умножения вектора на скаляр"""
    a = [1, 2, 0, -4]
    b = 3
    exp = [3, 6, 0, -12]
    res = vec.multiply_by_scalar(a, b)
    assert res == exp


def test_multiply():
    """Тест умножения векторов"""
    a = [1, 2, 3]
    b = [2, 2, 3]
    exp = 15
    res = vec.multiply(a, b)
    assert res == exp


def test_normalize():
    """Тест нормализации векторов"""
    a = [3, 5, 8]
    exp = [3/98 ** 0.5, 5/98 ** 0.5, 8/98 ** 0.5]
    res = vec.normalize(a)
    assert res == exp


def test_equality():
    """Тест равенства векторов"""
    a = [3, 5, 8]
    b = [3, 5, 8]
    exp = True
    res = vec.equality(a, b)
    assert res == exp


def test_equality_accuracy():
    """Тест равентсва векторов с заданоой точностью"""
    a = [3, 5, 8]
    b = [3, 5, 8]
    eps = 1 / 10 ** 99
    exp = True
    res = vec.equality_accuracy(a, b, eps)
    assert res == exp


def test_cos_angle():
    """Тест косинуса угла"""
    a = [3, 4]
    b = [4, 3]
    exp = 0.96
    res = vec.cos_between(a, b)
    assert res == exp


def test_angle_radians():
    """Тест угла между векторами в радианах с точностью в 0.01"""
    a = [3, 4]
    b = [4, 3]
    exp = 0.28
    res = vec.angle_between_radians(a, b)
    assert abs(res - exp) < 0.01


def test_angle_degrees():
    """Тест угла между векторами в градусах с точностью в 0.01"""
    a = [3, 4]
    b = [4, 3]
    exp = 16.26
    res = vec.angle_between_degrees(a, b)
    assert abs(res - exp) < 0.01


def test_oppositely_directed():
    """Тест противоположно направленных векторов"""
    a = [2, 3]
    b = [-2, -3]
    exp = True
    res = vec.oppositely_directed(a, b)
    assert res == exp


def test_co_directed():
    """Тест сонаправленных векторов"""
    a = [2, 3]
    b = [4, 6]
    exp = True
    res = vec.co_directed(a, b)
    assert res == exp


def test_change_direction():
    """Тест изменения направления вектора на противоположный"""
    a = [5, -2, 0, 3]
    exp = [-5, 2, 0, -3]
    res = vec.change_direction(a)
    assert res == exp


def test_collinear():
    """Тест коллинеарности векторов"""
    a = [2, 5]
    b = [-2, -5]
    exp = True
    res = vec.collinear(a, b)
    assert res == exp


def test_projection_scalar():
    """Тест скалярной проекции вектора на вектор"""
    a = [1, 2]
    b = [3, 4]
    exp = 2.2
    res = vec.projection_scalar(a, b)
    assert res == exp


def test_projection_vector():
    """Тест векторной проекции вектора на вектор"""
    a = [4, 5]
    b = [6, 0]
    exp = [4, 0]
    res = vec.projection_vector(a, b)
    assert res == exp
