import approximation.approximation_module as apn
from packages.round.round_module import deep_round_points
from packages.list.list_converter import deep_convert_list_to_tuple


def test_lsm_matrix_form_one_unknown():
    """Тест метода наименьших квадратов в матричном виде с одним неизвестным"""
    a = [[2],
         [3]]
    b = [[4],
         [9]]
    exp = (2.69, 2.77)
    data_vectors, data_points = apn.lsm_matrix_form_one_unknown(a, b)
    res_round = deep_round_points(data_points[0], 2)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp


def test_lsm_matrix_form_two_unknowns():
    """Тест метода наименьших квадратов в матричном виде с двумя неизвестными"""
    a = [[2, 3],
         [3, 3],
         [4, 7]]
    b = [[7],
         [7],
         [3]]
    exp = (4.68, -2.06, 17.02)
    data_vectors, data_points = apn.lsm_matrix_form_two_unknowns(a, b)
    res_round = deep_round_points(data_points[0], 2)
    res_round_tuple = deep_convert_list_to_tuple(res_round)
    assert res_round_tuple == exp


def test_linear_approximation():
    """Тест линейной аппроксимации"""
    data_xy = [(1, 2),
               (3, 4),
               (3.5, 3),
               (6, 7)]
    list_x = [[1],
              [3],
              [5]]
    exp = [[1.66], [3.63], [5.60]]
    coordinate_vectors, list_y = apn.approximation_by_func(data_xy, list_x)
    res_round = deep_round_points(list_y, 2)
    assert res_round == exp


def test_approximation_by_2nd_degree_polynomial():
    """Тест аппроксимационного полиномома 2-й степени"""
    data_xy = [(1, 2),
               (3, 4),
               (3.5, 3),
               (6, 7)]
    list_x = [[1],
              [3],
              [5]]
    exp = [[2.09], [3.26], [5.46]]
    coordinate_vectors, list_y = apn.approximation_by_2nd_degree_polynomial(data_xy, list_x)
    res_round = deep_round_points(list_y, 2)
    assert res_round == exp


def test_approximation_by_3nd_degree_polynomial():
    """Тест аппроксимационного полиномома 3-й степени"""
    data_xy = [(1, 2),
               (3, 4),
               (3.5, 3),
               (6, 7)]
    list_x = [[1],
              [3],
              [5]]
    exp = [[2.00], [4.00], [2.16]]
    coordinate_vectors, list_y = apn.approximation_by_3nd_degree_polynomial(data_xy, list_x)
    res_round = deep_round_points(list_y, 2)
    assert res_round == exp


def test_approximation_by_arbitrary_function():
    """Тест аппроксимации произвольной функции степени"""
    data_xy = [(1, 2),
               (3, 4),
               (3.5, 3),
               (6, 7)]
    list_x = [[1],
              [3],
              [5]]
    exp = [[2.07], [3.26], [5.69]]
    coordinate_vectors, list_y = apn.approximation_by_arbitrary_function(data_xy, list_x)
    res_round = deep_round_points(list_y, 2)
    assert res_round == exp
