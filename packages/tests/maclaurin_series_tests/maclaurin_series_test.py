import maclaurin_series.maclaurin_series_module as mc
from math import e


def test_maclaurin_series_expansion_ex():
    """Тест разложения в ряд Маклорена e^x"""
    n = 25
    eps = 10
    x = 1
    exp = (x, round(e, eps))
    coordinates = mc.maclaurin_series_expansion(mc.process_ex, n, x, x)
    coordinate = (x, round(coordinates[0][1], eps))
    assert coordinate == exp


def test_maclaurin_series_expansion_sin():
    """Тест разложения в ряд Маклорена sin(x)"""
    n = 25
    eps = 1
    x = 1.5
    exp = (x, 1)
    coordinates = mc.maclaurin_series_expansion(mc.process_sin, n, x, x)
    coordinate = (x, round(coordinates[0][1], eps))
    assert coordinate == exp


def test_maclaurin_series_expansion_cos():
    """Тест разложения в ряд Маклорена cos(x)"""
    n = 25
    eps = 1
    x = 0
    exp = (x, 1)
    coordinates = mc.maclaurin_series_expansion(mc.process_cos, n, x, x)
    coordinate = (x, round(coordinates[0][1], eps))
    assert coordinate == exp


def test_maclaurin_series_expansion_arc_sin():
    """Тест разложения в ряд Маклорена arcsin(x)"""
    n = 25
    eps = 1
    x = 0.5
    exp = (x, 0.5)
    coordinates = mc.maclaurin_series_expansion(mc.process_arc_sin, n, x, x)
    coordinate = (x, round(coordinates[0][1], eps))
    assert coordinate == exp


def test_maclaurin_series_expansion_arc_cos():
    """Тест разложения в ряд Маклорена arccos(x)"""
    n = 25
    eps = 1
    x = 0.5
    exp = (x, 1)
    coordinates = mc.maclaurin_series_expansion(mc.process_arc_cos, n, x, x)
    coordinate = (x, round(coordinates[0][1], eps))
    assert coordinate == exp
