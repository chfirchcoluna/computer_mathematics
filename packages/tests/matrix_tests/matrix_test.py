import matrix.matrix_module as mtx


def test_add():
    """Тест сложения матриц"""
    a = [[1, 2, 2], [3, 3, 2]]
    b = [[4, 8, 2], [0, 0, -1]]
    exp = [[5, 10, 4], [3, 3, 1]]
    res = mtx.add_matrix(a, b)
    assert res == exp


def test_transposition():
    """Тест транспозиции матриц"""
    a = [[2, 3, 5], [5, 7, 6], [5, 7, 6]]
    exp = [[2, 5, 5], [3, 7, 7], [5, 6, 6]]
    res = mtx.transposition(a)
    assert res == exp


def test_multiply_matrix_by_scalar():
    """Тест умножение матрицы на скаляр"""
    a = [[1, 2], [3, 4]]
    b = 2
    exp = [[2, 4], [6, 8]]
    res = mtx.multiply_matrix_by_scalar(a, b)
    assert res == exp


def test_multiply_matrix():
    """Тест умножения матриц"""
    a = [[1, 2], [3, 4], [5, 6]]
    b = [[2, 3, 2], [4, 5, 2]]
    exp = [[10, 13, 6], [22, 29, 14], [34, 45, 22]]
    res = mtx.multiply_matrix(a, b)
    assert res == exp


def test_get_row():
    """Тест взятия строки"""
    a = [[10, 13, 6], [22, 29, 14], [34, 45, 22]]
    b = 2
    exp = [34, 45, 22]
    res = mtx.get_row(a, b)
    assert res == exp


def test_get_column():
    """Тест взятия столбца"""
    a = [[10, 13, 6], [22, 29, 14], [34, 45, 22]]
    b = 2
    exp = [6, 14, 22]
    res = mtx.get_column(a, b)
    assert res == exp


def test_rearranging_rows():
    """Тест перестановки строк"""
    a = [[3, 2, 1], [2, 2, 2], [0, 0, 0]]
    b = 0
    c = 2
    exp = [[0, 0, 0], [2, 2, 2], [3, 2, 1]]
    res = mtx.rearranging_rows(a, b, c)
    assert res == exp


def test_multiply_row_matrix_by_scalar():
    """Тест умножения строки матрицы на скаляр"""
    a = [[10, 13, 6], [22, 29, 14], [34, 45, 22]]
    b = 1
    c = 2
    exp = [[10, 13, 6], [44, 58, 28], [34, 45, 22]]
    res = mtx.multiply_row_matrix_by_scalar(a, b, c)
    assert res == exp


def test_add_row_multiplied_by_scalar():
    """Тест сложения строки матрицы, умноженной на скаляр"""
    a = [[10, 4, 3], [22, 3, 4]]
    b = 0
    c = 0
    d = 3
    exp = [[40, 16, 12], [22, 3, 4]]
    res = mtx.add_row_multiplied_by_scalar(a, b, c, d)
    assert res == exp


def test_sub_row_multiplied_by_scalar():
    """Тест вычитания строки матрицы, умноженной на скаляр"""
    a = [[10, 4, 3], [22, 3, 4]]
    b = 1
    c = 1
    d = 3
    exp = [[10, 4, 3], [-44, -6, -8]]
    res = mtx.sub_row_multiplied_by_scalar(a, b, c, d)
    assert res == exp
