def deep_round_points(points, round_number: int = 5):
    """Получение глупокой копии точки(-ек) в виде списка(-ов) округлённых значений или копии округлённого значения"""
    if not isinstance(points, tuple) and not isinstance(points, list):
        return round(points, round_number)  # number
    else:
        return [deep_round_points(elem, round_number) for elem in points]
