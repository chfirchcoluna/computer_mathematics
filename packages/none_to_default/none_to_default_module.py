def none_to_default(param, default_param=0.0):
    """Установка параметра по умолчанию [0.0] при отсутствии параметра"""
    return param if param is not None else default_param
