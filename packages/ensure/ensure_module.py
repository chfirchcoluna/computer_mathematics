from packages.copy.deep_copy_module import deep_copy_matrix


def ensure_value(is_ok, msg=''):
    """Выброс исключения значения"""
    if not is_ok:
        raise ValueError(msg)


def ensure_type(is_ok, msg=''):
    """Выброс исключения типа"""
    if not is_ok:
        raise TypeError(msg)


def is_not_none(arg):
    """Проверка на не None"""
    return arg is not None


def ensure_is_not_none(arg):
    """Убеждение на не None"""
    ensure_value(is_not_none(arg), 'there is no system solution')


def ensure_need_do_copy_matrix(matrix, do_copy):
    """Убеждение в том, что требуется копия матрицы"""
    if do_copy:
        return deep_copy_matrix(matrix)
    return matrix
