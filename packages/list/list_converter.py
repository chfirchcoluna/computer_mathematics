def deep_convert_list_to_tuple(points: list):
    """Получение глубокой копии преобразования всех списков в кортежи"""
    if not isinstance(points, tuple) and not isinstance(points, list):
        return points  # number
    else:
        return tuple(deep_convert_list_to_tuple(elem) for elem in points)
