def str_to_float_number(s):
    """Преобразование строки в вещественное число"""
    a = float(s)
    return a


def str_to_int_number(s):
    """Преобразование строки в целое число"""
    a = int(s)
    return a
