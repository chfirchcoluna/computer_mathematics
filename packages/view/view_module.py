def show_array(res, msg=''):
    """Вывод массива на экран. View."""
    if res is not None and msg is not None:
        if (isinstance(res, tuple) or isinstance(res, list)) and None in res:
            return
        if (isinstance(msg, tuple) or isinstance(msg, list)) and None in msg:
            return
        print(msg)
        print(res)


def show_error(err, msg=''):
    """Вывод ошибки на экран. View."""
    print(msg)
    print(err)


def show_info(msg=''):
    """Вывод сообщения на экран. View."""
    print(msg)


def show_info_about_none_params(*params):
    """Информация об одном из None параметров"""
    if None in params:
        show_info('Один из параметров пропущен или указан неверно. Установлено(ы) значение(я) по умолчанию.')
