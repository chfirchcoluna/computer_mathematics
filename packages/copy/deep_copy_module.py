def deep_copy_matrix(matrix):
    """Получение копии массива"""
    if not isinstance(matrix, list):
        return matrix  # number
    else:
        return [deep_copy_matrix(elem) for elem in matrix]
