def convert_data_x_y(*points):
    """Конвертирует список переданных точек в списки данных x, y для библиотеки matplotlib"""
    data_x, data_y = [], []
    for element in points:
        if isinstance(element, list):
            for i in range(len(element)):
                data_x.append(element[i][0])
                data_y.append(element[i][1])
        else:
            data_x.append(element[0])
            data_y.append(element[1])
    return data_x, data_y


def convert_data_x_y_z(*points):
    """Конвертирует список переданных точек в списки данных x, y, z для библиотеки matplotlib"""
    data_x, data_y, data_z = [], [], []
    for element in points:
        if isinstance(element, list):
            for i in range(len(element)):
                data_x.append(element[i][0])
                data_y.append(element[i][1])
                data_z.append(element[i][2])
        else:
            data_x.append(element[0])
            data_y.append(element[1])
            data_z.append(element[2])
    return data_x, data_y, data_z
