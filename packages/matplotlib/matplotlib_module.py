import matplotlib.pyplot as plt


def show_3d_figure_result_matplotlib(list_xyz_vectors: list, list_xyz_points: list = None):
    """Вывод 3D фигуры на экран в виде поверхности."""
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    if list_xyz_vectors is not None:
        sx = [e for e in list_xyz_vectors[0]]
        sy = [e for e in list_xyz_vectors[1]]
        sz = [e for e in list_xyz_vectors[2]]
        surf = ax.plot_trisurf(sx, sy, sz, cmap=plt.cm.jet, linewidth=0, antialiased=False, alpha=0.6)
        fig.colorbar(surf, shrink=0.5, aspect=5)

    if list_xyz_points is not None:
        px = [e for e in list_xyz_points[0]]
        py = [e for e in list_xyz_points[1]]
        pz = [e for e in list_xyz_points[2]]
        ax.scatter3D(px, py, pz, color='RED', s=200)

    ax.set_title('Surface')
    plt.show()


def show_chart_2d_figure_result_matplotlib(list_xy_vectors: list, list_xy_points: list = None,
                                           marker_vectors: str = 'o', marker_points: str = 'o'):
    """Вывод 2D фигуры на экран."""
    if list_xy_vectors is not None:
        vx = [e for e in list_xy_vectors[0]]
        vy = [e for e in list_xy_vectors[1]]
        plt.plot(vx, vy, marker=marker_vectors)
    if list_xy_points is not None:
        px = [e for e in list_xy_points[0]]
        py = [e for e in list_xy_points[1]]
        plt.scatter(px, py, marker=marker_points, color='ORANGE')
    plt.title('Chart')
    plt.get_current_fig_manager().set_window_title('Chart')
    plt.grid()
    plt.show()


def show_chart_2d_figure_result_matplotlib_more_points(list_xy_vectors: list, list_xy_points: list = None,
                                                       additional_list_xy_points: list = None,
                                                       marker_vectors: str = 'o', marker_points: str = 'o'):
    """Вывод 2D фигуры на экран. Дополнительные точки."""
    if additional_list_xy_points is not None:
        apx = [e for e in additional_list_xy_points[0]]
        apy = [e for e in additional_list_xy_points[1]]
        plt.scatter(apx, apy, marker=marker_points)
    if list_xy_vectors is not None:
        vx = [e for e in list_xy_vectors[0]]
        vy = [e for e in list_xy_vectors[1]]
        plt.plot(vx, vy, marker=marker_vectors)
    if list_xy_points is not None:
        px = [e for e in list_xy_points[0]]
        py = [e for e in list_xy_points[1]]
        plt.scatter(px, py, marker=marker_points, color='ORANGE')
    plt.title('Chart')
    plt.get_current_fig_manager().set_window_title('Chart')
    plt.grid()
    plt.show()


def show_chart_2d_result_matplotlib_linear_approximation(list_xy_vectors: list, list_xy_points: list = None,
                                                         marker_vectors: str = 'o', marker_points: str = 'o'):
    """Вывод 2D фигуры на экран. Линейная аппроксимация. Бесконечная линия"""
    if list_xy_vectors is not None:
        vx = [e for e in list_xy_vectors[0]]
        vy = [e for e in list_xy_vectors[1]]
        x1y1 = (list_xy_vectors[0][0][0], list_xy_vectors[1][0][0])
        x2y2 = (list_xy_vectors[0][1][0], list_xy_vectors[1][1][0])
        plt.axline(x1y1, x2y2, marker=marker_vectors)
        plt.scatter(vx, vy, marker=marker_points, color='ORANGE')
    if list_xy_points is not None:
        px = [e for e in list_xy_points[0]]
        py = [e for e in list_xy_points[1]]
        plt.scatter(px, py, marker=marker_points)
    plt.title('Chart')
    plt.get_current_fig_manager().set_window_title('Chart')
    plt.grid()
    plt.show()


def show_two_chart_2d_figure_result_matplotlib(list_xy_vectors: list, list_xy_vectors_additional: list = None,
                                               marker_vectors: str = 'o', marker_points_additional: str = 'o'):
    """Вывод 2D фигуры на экран."""
    if list_xy_vectors is not None:
        vx = [e for e in list_xy_vectors[0]]
        vy = [e for e in list_xy_vectors[1]]
        plt.plot(vx, vy, marker=marker_vectors)
    if list_xy_vectors_additional is not None:
        vax = [e for e in list_xy_vectors_additional[0]]
        vay = [e for e in list_xy_vectors_additional[1]]
        plt.plot(vax, vay, marker=marker_points_additional, color='ORANGE')
    plt.ylim(-2, 5)
    plt.title('Chart')
    plt.get_current_fig_manager().set_window_title('Chart')
    plt.grid()
    plt.show()
