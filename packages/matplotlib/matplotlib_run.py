from packages.ensure.ensure_module import ensure_type
from packages.view.view_module import show_error


def is_right_type_for_matplotlib(*objs):
    """Правильный тип. Условно Controller."""
    return True not in [isinstance(obj, float) or isinstance(obj, bool) or isinstance(obj, int) for obj in objs]


def ensure_is_right_type_for_matplotlib(*objs):
    """Проверка на правильный тип для вызова библиотеки matplotlib. Условно Controller."""
    ensure_type(is_right_type_for_matplotlib(*objs), 'unable to display non-sheet type')


def handle_non_right_type_for_matplotlib_exception(*params):
    """Отлов брошенного исключения неправильного типа переменной для работы с matplotlib. Условно Controller."""
    try:
        ensure_is_right_type_for_matplotlib(*params)
    except TypeError as err:
        return show_error(err, 'Ошибка неправильного типа переменной')
    return True


def try_run_matplotlib(func, *params):
    """Проверка вызова библиотеки matplotlib. Условно Controller."""
    try:
        func(*params)
    except Exception as err:
        return show_error(err, 'Ошибка при вызове библиотеки matplotlib')
