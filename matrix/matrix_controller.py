from matrix.matrix_module import add_matrix, sub_matrix, transposition, multiply_matrix_by_scalar, multiply_matrix, get_row, \
    get_column, rearranging_rows, multiply_row_matrix_by_scalar, add_row_multiplied_by_scalar, \
    sub_row_multiplied_by_scalar
from packages.view.view_module import show_array, show_error, show_info_about_none_params
from packages.scalar.scalar_converter import str_to_float_number, str_to_int_number
from packages.calculate.try_calculate import try_calculate


MATRIX_0 = [[1, 2],
            [3, 4]]
MATRIX_1 = [[1, 2],
            [3, 4]]


def add_matrix_action():
    """Сложение матриц. Действие."""
    a, b = MATRIX_0, MATRIX_1  # Получение данных для расчетов
    res = try_calculate(add_matrix, a, b)
    show_array(res, 'Сложение матриц')


def sub_matrix_action():
    """Вычитание матриц. Действие."""
    a, b = MATRIX_0, MATRIX_1
    res = try_calculate(sub_matrix, a, b)
    show_array(res, 'Вычитание матриц')


def transposition_action():
    """Транспозиция матрицы. Действие."""
    a = MATRIX_0
    res = try_calculate(transposition, a)
    show_array(res, 'Транспозиция матрицы')


def multiply_matrix_by_scalar_action():
    """Умножение матрицы на скаляр. Действие."""
    a = MATRIX_0
    b = input_one_number('Введите скаляр. Можно пропустить - [Enter]\n')
    show_info_about_none_params(b)
    res = try_calculate(multiply_matrix_by_scalar, a, b)
    show_array(res, 'Умножение матрицы на скаляр')


def multiply_matrix_action():
    """Перемножение матриц. Действие."""
    a, b = MATRIX_0, MATRIX_1
    res = try_calculate(multiply_matrix, a, b)
    show_array(res, 'Перемножение матриц')


def get_row_action():
    """Получение строки матрицы. Действие."""
    a = MATRIX_0
    b = input_one_number('Введите индекс строки. Можно пропустить - [Enter]\n', int)
    cp = input_one_number('Нужно копировать или составить ссылку? [True/False]. Можно пропустить - [Enter]\n', bool)
    show_info_about_none_params(b, cp)
    res = try_calculate(get_row, a, b, cp)
    show_array(res, 'Получение строки по индексу')


def get_column_action():
    """Получение столбца матрицы. Действие."""
    a = MATRIX_0
    b = input_one_number('Введите индекс столбца. Можно пропустить - [Enter]\n', int)
    cp = input_one_number('Нужно копировать или составить ссылку? [True/False]. Можно пропустить - [Enter]\n', bool)
    show_info_about_none_params(b, cp)
    res = try_calculate(get_column, a, b, cp)
    show_array(res, 'Получение столбца по индексу')


def rearranging_rows_action():
    """Перестановка строк. Действие."""
    a = MATRIX_0
    b = input_one_number('Введение индекс откуда переставить. Можно пропустить - [Enter]\n', int)
    c = input_one_number('Введите индекс куда переставить. Можно пропустить - [Enter]\n', int)
    cp = input_one_number('Нужно копировать или составить ссылку? [True/False]. Можно пропустить - [Enter]\n', bool)
    show_info_about_none_params(b, c, cp)
    res = try_calculate(rearranging_rows, a, b, c, cp)
    show_array(res, 'Перестановка строк')


def multiply_row_matrix_by_scalar_action():
    """Умножение выбранной строки на скаляр. Действие."""
    a = MATRIX_0
    b = input_one_number('Введение какую по индексу строку надо умножить. Можно пропустить - [Enter]\n', int)
    c = input_one_number('Введите на сколько умножить. Можно пропустить - [Enter]\n')
    cp = input_one_number('Нужно копировать или составить ссылку? [True/False]. Можно пропустить - [Enter]\n', bool)
    show_info_about_none_params(b, c, cp)
    res = try_calculate(multiply_row_matrix_by_scalar, a, b, c, cp)
    show_array(res, 'Умножение выбранной строки на скаляр')


def add_row_multiplied_by_scalar_action():
    """Сложение выбранной строки с другой выбранной строкой, умноженной на скаляр. Действие."""
    a = MATRIX_0
    b = input_one_number('Введите индекс выбранной начальной строки. Можно пропустить - [Enter]\n', int)
    c = input_one_number('Введите индекс прибавляемой строки. Можно пропустить - [Enter]\n', int)
    d = input_one_number('Введите число умножения прибавляемой строки. Можно пропустить - [Enter]\n')
    cp = input_one_number('Нужно копировать или составить ссылку? [True/False]. Можно пропустить - [Enter]\n', bool)
    show_info_about_none_params(b, c, d, cp)
    res = try_calculate(add_row_multiplied_by_scalar, a, b, c, d, cp)
    show_array(res, 'Сложение строк, умноженных на число')


def sub_row_multiplied_by_scalar_action():
    """Вычитание выбранной строки с другой выбранной строкой, умноженной на скаляр. Действие."""
    a = MATRIX_0
    b = input_one_number('Введите индекс выбранной начальной строки. Можно пропустить - [Enter]\n', int)
    c = input_one_number('Введите индекс вычитаемой строки. Можно пропустить - [Enter]\n', int)
    d = input_one_number('Введите число умножения вычитаемой строки. Можно пропустить - [Enter]\n')
    cp = input_one_number('Нужно копировать или составить ссылку? [True/False]. Можно пропустить - [Enter]\n', bool)
    show_info_about_none_params(b, c, d, cp)
    res = try_calculate(sub_row_multiplied_by_scalar, a, b, c, d, cp)
    show_array(res, 'Вычитание строк, умноженных на число')


def input_one_number(msg='Введите число: ', type_number=float):
    """Ввод одного числа с клавиатуры. Условно Controller."""
    try:
        s = input(msg)
        if s == '' or s == 'False':
            return None
        elif type_number is int:
            return str_to_int_number(s)
        elif type_number is bool:
            return True
        return str_to_float_number(s)
    except ValueError as err:
        return show_error(err, 'Ошибка при получении данных')


def get_actions():
    """Получение доступных действий"""
    return [add_matrix_action,
            sub_matrix_action,
            transposition_action,
            multiply_matrix_by_scalar_action,
            multiply_matrix_action,
            get_row_action,
            get_column_action,
            rearranging_rows_action,
            multiply_row_matrix_by_scalar_action,
            add_row_multiplied_by_scalar_action,
            sub_row_multiplied_by_scalar_action]
