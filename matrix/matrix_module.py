from vector.vector_module import add, sub, multiply_by_scalar, multiply, is_equal_len
from packages.ensure.ensure_module import ensure_value
from packages.none_to_default.none_to_default_module import none_to_default
from packages.ensure.ensure_module import ensure_need_do_copy_matrix


def create_extended_matrix_system(a, b):
    """Создание расшиенной версии матрицы"""
    return [x + y for x, y in zip(a, b)]


def ensure_is_equal_len(a, b):
    """Проверка на равенство размерностей матриц"""
    ensure_value(is_equal_len(a, b), 'different length of matrices')


def ensure_is_equal_columns_first_matrix_to_rows_second_matrix(a, b):
    """Проверка на равенство количества столбцов первой матрицы и строк второй матрицы"""
    ensure_value(is_equal_len(a, b), 'the number of columns of matrix A is not equal to the number of rows of matrix B')


def add_matrix(a, b):
    """Сложение матриц"""
    ensure_is_equal_len(a, b)
    return [add(x, y) for x, y in zip(a, b)]


def sub_matrix(a, b):
    """Вычитание матриц"""
    ensure_is_equal_len(a, b)
    return [sub(x, y) for x, y in zip(a, b)]


def transposition(a):
    """Транспонирование матрицы"""
    return [list(e) for e in zip(*a)]


def multiply_matrix_by_scalar(a, s=1):
    """Умножение матрицы на скаляр"""
    s = none_to_default(s, 1)
    return [multiply_by_scalar(e, s) for e in a]


def multiply_matrix(a, b):
    """Умножение матриц"""
    ensure_is_equal_columns_first_matrix_to_rows_second_matrix(a[0], b)
    return transposition([[multiply(e, transposition(b)[i]) for e in a] for i in range(max([len(i) for i in b]))])


def get_row(a, index=0, do_copy=True):
    """Получение строки по индексу"""
    index = none_to_default(index, 0)
    new_a = ensure_need_do_copy_matrix(a, do_copy)
    return new_a[index]


def get_column(a, index=0, do_copy=True):
    """Получение столбца по индексу"""
    index = none_to_default(index, 0)
    new_a = ensure_need_do_copy_matrix(a, do_copy)
    return transposition(new_a)[index]


def rearranging_rows(a, index_from=0, index_to=0, do_copy=True):
    """Перестановка строк"""
    index_from, index_to = none_to_default(index_from, 0), none_to_default(index_to, 0)
    new_a = ensure_need_do_copy_matrix(a, do_copy)
    new_a[index_from], new_a[index_to] = new_a[index_to], new_a[index_from]
    return new_a


def multiply_row_matrix_by_scalar(a, index=0, s=0, do_copy=True):
    """Умножение выбранной строки на скаляр"""
    index, s = none_to_default(index, 0), none_to_default(s, 1)
    new_a = ensure_need_do_copy_matrix(a, do_copy)
    new_a[index] = [e * s for e in new_a[index]]
    return new_a


def add_row_multiplied_by_scalar(a, index_to=0, index_from=0, s=1, do_copy=True):
    """Сложение выбранной строки с другой выбранной строкой, умноженной на скаляр"""
    index_to, index_from, s = none_to_default(index_to, 0), none_to_default(index_from, 0), none_to_default(s, 1)
    new_a = ensure_need_do_copy_matrix(a, do_copy)
    new_a[index_to] = add(new_a[index_to], multiply_by_scalar(new_a[index_from], s))
    return new_a


def sub_row_multiplied_by_scalar(a, index_to=0, index_from=0, s=1, do_copy=True):
    """Вычитание выбранной строки с другой выбранной строкой, умноженной на скаляр"""
    index_to, index_from, s = none_to_default(index_to, 0), none_to_default(index_from, 0), none_to_default(s, 1)
    new_a = ensure_need_do_copy_matrix(a, do_copy)
    new_a[index_to] = sub(new_a[index_to], multiply_by_scalar(new_a[index_from], s))
    return new_a
